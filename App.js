import React from 'react';
import NavigationApp from './app/navigation';
import {Provider} from 'react-redux';
import {store} from './app/store/store';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <NavigationApp />
      </Provider>
    );
  }
}

export default App;
