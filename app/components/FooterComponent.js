import React from 'react';
import {connect} from 'react-redux';
import {ActivityIndicator} from 'react-native';

class FooterComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return this.props.isLoading ? <ActivityIndicator size={'large'} /> : null;
  }
}

const mapStateToProps = state => ({
  isLoading: state.isLoadingMore,
});

export default connect(mapStateToProps)(FooterComponent);
