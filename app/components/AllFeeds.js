import React from 'react';
import {FlatList, View, ActivityIndicator, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import FooterComponent from './FooterComponent';
import ItemComponent from './ItemComponent';
class AllFeeds extends React.Component {
  constructor(props) {
    super(props);
    this.pageNumber = 0;
  }

  componentDidMount(): void {
    this.props.fetchData();
  }

  render() {
    let {loading, data} = this.props;
    return loading ? (
      <View style={styles.loading}>
        <ActivityIndicator size={'large'} />
      </View>
    ) : (
      <FlatList
        data={data}
        renderItem={({item}) => (
          <ItemComponent url={item.url} title={item.title} />
        )}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        keyExtractor={item => item.id}
        ListFooterComponent={<FooterComponent />}
        onEndReachedThreshold={0.1}
        onEndReached={() => {
          this.pageNumber = this.pageNumber + 1;
          this.props.loadMore(this.pageNumber);
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: 'center',
  },
  separator: {
    height: 10,
    backgroundColor: 'rgba(0,0,0,0.4)',
    marginBottom: 25,
    marginTop: 25,
  },
});

const mapStateToProps = state => ({
  data: state.data,
  loading: state.loading,
});

const mapDispatchToProps = dispatch => ({
  fetchData: () => dispatch({type: 'FETCH_DATA'}),
  loadMore: pageNumber => dispatch({type: 'LOAD_MORE', pageNumber: pageNumber}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AllFeeds);
