import React from 'react';
import {SearchBar} from 'react-native-elements';
import {connect} from 'react-redux';

class MySearchBar extends React.Component {
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({search});
  };

  render() {
    const {search} = this.state;

    return (
      <SearchBar
        round
        searchIcon={{
          size: 24,
          onPress: () => this.props.dispatch({type: 'FETCH_DATA'}),
        }}
        platform={'default'}
        lightTheme={true}
        placeholder="Type Here..."
        onChangeText={this.updateSearch}
        value={search}
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch,
});

export default connect(
  null,
  mapDispatchToProps,
)(MySearchBar);
