import React from 'react';
import WebView from 'react-native-webview';
import {Text, View, StyleSheet, Dimensions} from 'react-native';
import getVideo from '../utils';

export default class ItemComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {url, title} = this.props;
    return (
      <View style={styles.view}>
        <WebView
          source={{
            uri: getVideo(url),
          }}
        />
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  view: {
    backgroundColor: 'black',
    height: Dimensions.get('screen').width,
  },
  title: {
    fontSize: 18,
    margin: 20,
    color: 'white',
  },
});
