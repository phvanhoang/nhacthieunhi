import {connect} from 'react-redux';
import React from 'react';
import {ActivityIndicator, StyleSheet} from 'react-native';

class AllFeedsLoading extends React.Component {
  render() {
    return this.props.loading ? (
      <ActivityIndicator style={style.loading} size={'small'} />
    ) : null;
  }
}

const style = StyleSheet.create({
  loading: {
    alignContent: 'center',
  },
});

const mapStateToProps = state => ({loading: state.loading});
export default connect(mapStateToProps)(AllFeedsLoading);
