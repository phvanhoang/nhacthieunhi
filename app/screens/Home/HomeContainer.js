import HomeView from './HomeView';
import React from 'react';

class HomeContainer extends React.Component {
  render() {
    return <HomeView />;
  }
}

export default HomeContainer;
