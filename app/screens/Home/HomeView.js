import React from 'react';
import {SearchBar} from 'react-native-elements';
import {View} from 'react-native';
import AllFeeds from '../../components/AllFeeds';
import AllFeedsLoading from '../../components/AllFeedsLoading';
import MySearchBar from '../../components/MySearchBar';

export default class HomeView extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <MySearchBar />
        <AllFeeds />
      </View>
    );
  }
}
