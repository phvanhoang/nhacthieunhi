import ApiConstants from './ApiConstants';
export default function api(path, method) {
  let options;
  options = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: method,
  };

  return fetch(ApiConstants.BASE_URL + path, options)
    .then(json => json.json())
    .then(json => json.content);
}
