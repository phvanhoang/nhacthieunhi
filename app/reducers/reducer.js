import * as types from '../actions/types';

const initialState = {
  data: [],
  loading: false,
  error: false,
  isLoadingMore: false,
  loadMoreSuccess: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.FETCHING_DATA:
      return {...state, loading: true, error: false};
    case types.FETCHING_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.data,
        error: false,
      };
    case types.FETCHING_FAIL:
      return {...state, loading: false, error: true};
    case types.LOAD_MORE_DATA:
      return {...state, isLoadingMore: true};
    case types.LOAD_MORE_SUCCESS:
      return {
        ...state,
        isLoadingMore: false,
        data: [...state.data, ...action.data],
      };
    default:
      return state;
  }
}
