import NavigationStack from './NavigationStack';
import React from 'react';

class NavigationApp extends React.Component {
  render() {
    return <NavigationStack />;
  }
}

export default NavigationApp;
