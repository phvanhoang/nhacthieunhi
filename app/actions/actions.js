import * as types from './types';

export function startFetching() {
  return {
    type: types.FETCHING_START,
  };
}

export function fetchingData() {
  return {
    type: types.FETCHING_DATA,
  };
}

export function getDataSuccess(data) {
  return {
    type: types.FETCHING_SUCCESS,
    data,
  };
}

export function getDataFail() {
  return {
    type: types.FETCHING_FAIL,
  };
}

export function getLoadMoreData() {
  return {
    type: types.LOAD_MORE_DATA,
  };
}
