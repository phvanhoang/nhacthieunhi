import {call, put, takeEvery, take, fork} from 'redux-saga/effects';
import api from '../api/index';
import * as types from '../actions/types';

function* fetchData() {
  yield takeEvery('FETCH_DATA', fetchDataFlow);
}

function* fetchDataFlow() {
  try {
    yield put({type: types.FETCHING_DATA});
    const data = yield call(
      api,
      'feeds/searchByCommunityName?name=funnyvideo',
      'GET',
    );
    yield put({type: types.FETCHING_SUCCESS, data: data});
  } catch (e) {
    yield put({type: types.FETCHING_FAIL});
  }
}

function* loadMore() {
  while (true) {
    let {pageNumber} = yield take('LOAD_MORE');
    yield loadMoreFlow(pageNumber);
  }
}

function* loadMoreFlow(pageNumber) {
  yield put({type: types.LOAD_MORE_DATA});
  const data = yield call(
    api,
    'feeds/searchByCommunityName?name=funnyvideo&page=' + pageNumber,
    'GET',
  );
  yield put({type: types.LOAD_MORE_SUCCESS, data: data});
}

export default function* rootSaga() {
  yield fork(fetchData);
  yield fork(loadMore);
}
